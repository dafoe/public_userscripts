// ==UserScript==
// @name         YT Control Hider
// @namespace    http://tampermonkey.net/
// @version      0.1
// @description  Show/Hide the controls and annotation overlay in YT
// @author       You
// @match        https://www.youtube.com/*
// @grant        none
// ==/UserScript==

var ythidden = false;

(function() {
    'use strict';
    document.addEventListener("keydown", function(evt) {
        //console.log("userscript", evt);
        if (evt.key == "F9") {
            //console.log("flipping");
            ythidden ? ythidden = false : ythidden = true;
            document.querySelector(".ytp-chrome-bottom").hidden = ythidden;
            document.querySelector(".ytp-iv-player-content").hidden = ythidden;
        }
    }, true);
    // Your code here...
})();